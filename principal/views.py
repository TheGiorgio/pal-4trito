from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from .models import Perrito
# Create your views here.

def index(request):
    return render(request,'index.html',{'elementos':Perrito.objects.all()})

def login(request):
    return render(request,'login.html',{})

def formulario(request):
    return render(request,'formulario.html',{})

def registro_perrito(request):
    foto = request.FILES.get('foto','')
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perrito = Perrito(foto=foto,nombre=nombre,raza=raza,descripcion=descripcion,estado=estado)
    perrito.save()
    return HttpResponseRedirect('/')

def editar_perrito(request,id):
    perrito = Perrito.objects.get(pk=id)
    return render(request,'editar_perrito.html',{'perrito':perrito})

def perrito_editado(request,id):
    perrito = Perrito.objects.get(pk=id)
    foto = request.FILES.get('foto','')
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perrito.foto = foto
    perrito.nombre = nombre
    perrito.raza = raza
    perrito.descripcion = descripcion
    perrito.estado = estado
    perrito.save()
    return HttpResponseRedirect('/')
def eliminar_perrito(request,id):
    perrito = Perrito.objects.get(pk=id)
    perrito.delete()
    return HttpResponseRedirect('/')

def pwa_perris(request):
    return render(request,'pwa_perris.html',{})
    
